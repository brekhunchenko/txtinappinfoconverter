//
//  AppDelegate.swift
//  TxtInAppInfoConverter
//
//  Created by Yaroslav Brekhunchenko on 8/10/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit
import CHCSVParser
import AEXML

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let desktopURL = Bundle.main.url(forResource: "CadenzaIAPs2018_0", withExtension: "csv")
        
        let xmlDocument = AEXMLDocument()
        let inAppPurchasesXMLNode = xmlDocument.addChild(name: "in_app_purchases")
        if let array = NSArray(contentsOfCSVURL: desktopURL) as? Array<Array<Any>>{
            for i in 0 ..< array.count {
                //First object contains column keys.
                if i == 0 {
                    continue
                }
                
                let itemArray = array[i]
                let productId = itemArray[3] as! String
                let referenceName = itemArray[2] as! String
                let type = (itemArray[1] as! String).lowercased()
                let clearedForSale = (itemArray[4] as! String) == "YES" ? "true" : "false"
                let priceTier = itemArray[5] as! String
                let displayName = itemArray[6] as! String
                let description = itemArray[7] as! String
                let reviewNotes = itemArray[11] as! String
                
                let attributes = [String : String]()
                let inAppPurchaseXMLElement = inAppPurchasesXMLNode.addChild(name: "in_app_purchase", attributes: attributes)
                
                inAppPurchaseXMLElement.addChild(name: "product_id", value: productId)
                inAppPurchaseXMLElement.addChild(name: "reference_name", value: referenceName)
                inAppPurchaseXMLElement.addChild(name: "type", value: type)
                inAppPurchaseXMLElement.addChild(name: "review_notes", value: reviewNotes)

                let productsXml = inAppPurchaseXMLElement.addChild(name: "products")
                let productXml = productsXml.addChild(name: "product")
                productXml.addChild(name: "cleared_for_sale", value: clearedForSale, attributes: [String : String]())
                let intervalsXml = productXml.addChild(name: "intervals")
                let intervalXml = intervalsXml.addChild(name: "interval")
                intervalXml.addChild(name: "wholesale_price_tier", value: priceTier, attributes: [String : String]())
                
                let localesXml = inAppPurchaseXMLElement.addChild(name: "locales")
                let localeXml = localesXml.addChild(name: "locale", attributes: ["name" : "en-US"])
                localeXml.addChild(name: "title", value: displayName, attributes: [String : String]())
                localeXml.addChild(name: "description", value: description, attributes: [String : String]())

                let reviewScreenshotXml = inAppPurchaseXMLElement.addChild(name: "review_screenshot")
                reviewScreenshotXml.addChild(name: "size", value: "593494", attributes: [String : String]())
                reviewScreenshotXml.addChild(name: "file_name", value: "IAP_screenshot_2560×1600.png", attributes: [String : String]())
                reviewScreenshotXml.addChild(name: "checksum", value: "72a99191a18f9522c67a278041b1484c", attributes: ["type" : "md5"])
            }
        }
        
        print(xmlDocument.xml)
        
        return true
    }


}

